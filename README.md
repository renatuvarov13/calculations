## Запуск в Docker

- Проверям, что ничего не запущено на 80 порту, либо меняем порт в `docker-compose.yml` для контейнера `nginx`
- Заходим в корневую директорию, где находится Makefile
- Делаем `make init` для запуска контейнеров и установки пакетов Composer

## Остановка контейнеров

`make down`

## Запуск тестов

`make test`

## Запуск Bash в контейнере Php

`make bash`