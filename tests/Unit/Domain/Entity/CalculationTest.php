<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Entity;

use App\Domain\Entity\Calculation;
use App\Domain\Entity\Operation;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class CalculationTest extends TestCase
{
    private Generator $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function test_success(): void
    {
        $calculation = new Calculation(
            $this->faker->uuid,
            20,
            5,
            Operation::DIVIDED_BY,
        );

        $this->assertEquals(4, $calculation->calculate());
    }

    public function test_divided_by_zero(): void
    {
        $calculation = new Calculation(
            $this->faker->uuid,
            20,
            0,
            Operation::DIVIDED_BY,
        );

        $this->expectExceptionMessage('Division by zero');
        $calculation->calculate();
    }
}
