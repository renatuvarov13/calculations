<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Application\Persistence\Repository\CalculationRepositoryInterface;
use App\Application\Service\UuidGeneratorInterface;
use App\Infrastructure\Test\Fixture\CalculationFixture;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Uid\Uuid;

class CalculationControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private Serializer $serializer;
    protected AbstractDatabaseTool $databaseTool;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = $this->createClient();
        $this->serializer = $this->getContainer()->get('serializer');
        $this->databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->databaseTool);
    }

    public function test_index_success(): void
    {
        $crawler = $this->client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertCount(1, $crawler->filter('#root'));
    }

    public function test_create_calculation_success(): void
    {
        $uuid = (string)Uuid::v4();

        $mock = $this->createMock(UuidGeneratorInterface::class);
        $mock->method('generate')->willReturn($uuid);
        $this->getContainer()->set(UuidGeneratorInterface::class, $mock);

        $this->createCalculation($number1 = 1.2, $operation = 'plus', $number2 = 1);

        $this->assertResponseIsSuccessful();
        $this->assertEquals($uuid, $this->decode()['calculationId']);

        $entity = $this->getRepository()->findById($uuid);

        $this->assertEquals($entity->getNumber1(), $number1);
        $this->assertEquals($entity->getNumber2(), $number2);
        $this->assertEquals($entity->getOperation()->value, $operation);
        $this->assertFalse($entity->isCompleted());
    }

    public function test_create_calculation_invalid_input(): void
    {
        $this->createCalculation(operation: '%');

        $this->assertResponseIsUnprocessable();

        $errors = $this->decode()['errors'];
        $this->assertEquals('This field is missing.', $errors['number1'][0]);
        $this->assertEquals('This field is missing.', $errors['number2'][0]);
        $this->assertEquals('Unexpected operation type.', $errors['operation'][0]);
    }

    public function test_process(): void
    {
        $this->databaseTool->loadFixtures([
            CalculationFixture::class,
        ]);

        $entity = $this->getRepository()->getAll()[0];
        $this->assertFalse($entity->isCompleted());

        $this->client->request('GET', '/process');

        $calculation = $this->decode()['calculations'][0];
        $this->assertEquals($entity->getNumber1(), $calculation['number1']);
        $this->assertEquals($entity->getNumber2(), $calculation['number2']);
        $this->assertEquals($entity->getOperation()->value, $calculation['operation']);
        $this->assertEquals($entity->calculate(), $calculation['total']);

        $this->getContainer()->get(EntityManagerInterface::class)->clear();
        $entity = $this->getRepository()->getAll()[0];
        $this->assertTrue($entity->isCompleted());
    }

    private function createCalculation(mixed $number1 = null, mixed $operation = null, mixed $number2 = null): void
    {
        $body = [];
        if ($number1) {
            $body['number1'] = $number1;
        }
        if ($number2) {
            $body['number2'] = $number2;
        }
        if ($operation) {
            $body['operation'] = $operation;
        }

        $this->client->request(
            method: 'POST',
            uri: '/',
            server:             [
                'CONTENT_TYPE' => 'application/json',
                'ACCEPT' => 'application/json'
            ],
            content: $this->serializer->serialize($body, 'json')
        );
    }

    private function decode(): mixed
    {
        return $this->serializer->decode($this->client->getResponse()->getContent(), 'json');
    }

    private function getRepository(): CalculationRepositoryInterface
    {
        return $this->getContainer()->get(CalculationRepositoryInterface::class);
    }
}
