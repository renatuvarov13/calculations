init: up install migrate

up:
	docker-compose up -d --build

down:
	docker-compose down --remove-orphans

install:
	docker exec -it app-php composer i

migrate:
	docker exec -it app-php php bin/console do:mi:mi --no-interaction

bash:
	docker exec -it app-php bash

test:
	docker exec -it app-php composer test