<?php

declare(strict_types=1);

namespace App\Infrastructure\Test\Fixture;

use App\Domain\Entity\Calculation;
use App\Domain\Entity\Operation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class CalculationFixture extends Fixture
{
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $calculation = new Calculation(
            $this->faker->uuid,
            $this->faker->randomFloat(),
            $this->faker->randomFloat(min: 1.0),
            $this->faker->randomElement(Operation::cases()),
        );

        $manager->persist($calculation);
        $manager->flush();
    }
}