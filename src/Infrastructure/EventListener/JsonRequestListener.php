<?php

declare(strict_types=1);

namespace App\Infrastructure\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class JsonRequestListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'transformJsonBody',
        ];
    }

    public function transformJsonBody(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if ('application/json' === $request->headers->get('Content-Type')) {
            $data = json_decode($request->getContent(), true) ?: [];
            $request->request->replace($data);
        }
    }
}