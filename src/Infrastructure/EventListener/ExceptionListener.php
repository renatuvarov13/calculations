<?php

declare(strict_types=1);

namespace App\Infrastructure\EventListener;

use App\Infrastructure\Exception\ValidationException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Throwable;

class ExceptionListener implements EventSubscriberInterface
{
    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onException',
        ];
    }

    public function onException(ExceptionEvent $event): void
    {
        $e = $event->getThrowable();

        $message = $e->getMessage();

        if ($e instanceof ValidationException) {
            $message = [
                'errors' => $e->getErrors(),
            ];
        }

        $this->log($e);

        $event->setResponse(new JsonResponse($message, $e->getCode() ?: 500));
    }

    private function log(Throwable $e): void
    {
        if (!$e instanceof ValidationException) {
            $this->logger->error($e->getMessage());
        }
    }
}