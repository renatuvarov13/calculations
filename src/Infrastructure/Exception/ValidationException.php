<?php

declare(strict_types=1);

namespace App\Infrastructure\Exception;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class ValidationException extends Exception
{
    public function __construct(
        private readonly array $errors,
        string $message = 'Invalid input',
    ) {
        parent::__construct($message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}