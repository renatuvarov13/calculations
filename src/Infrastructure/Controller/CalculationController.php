<?php

declare(strict_types=1);

namespace App\Infrastructure\Controller;

use App\Application\Command\CreateCalculation\Command;
use App\Application\Query\GetCalculations\Query;
use App\Domain\Entity\Operation;
use App\Infrastructure\Service\Bus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalculationController extends AbstractController
{
    #[Route(path: '/', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('index.html.twig', [
            'options' => Operation::values(),
        ]);
    }

    #[Route(path: '/', methods: ['POST'])]
    #[ParamConverter('command', converter: 'calculation_param_converter')]
    public function create(Command $command, Bus $bus): JsonResponse
    {
        return $this->json([
            'calculationId' => $bus->handle($command),
        ]);
    }

    #[Route(path: '/process', methods: ['GET'])]
    public function process(Query $query, Bus $bus): JsonResponse
    {
        return $this->json([
            'calculations' => $bus->handle($query),
        ]);
    }
}