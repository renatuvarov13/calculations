<?php

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Application\Service\UuidGeneratorInterface;
use Symfony\Component\Uid\Uuid;

class UuidGenerator implements UuidGeneratorInterface
{
    public function generate(): string
    {
        return (string)Uuid::v4();
    }
}