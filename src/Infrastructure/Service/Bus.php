<?php

declare(strict_types=1);

namespace App\Infrastructure\Service;

use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class Bus
{
    public function __construct(private readonly MessageBusInterface $bus)
    {
    }

    public function handle(object $object): mixed
    {
        return $this->bus->dispatch($object)->last(HandledStamp::class)->getResult();
    }
}