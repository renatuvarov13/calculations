<?php

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Infrastructure\Exception\ValidationException;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator
{
    private readonly ConstraintViolationList $errors;

    public function __construct(private readonly ValidatorInterface $validator)
    {
        $this->errors = new ConstraintViolationList();
    }

    /**
     * @throws ValidationException
     */
    public function validate(mixed $data, array $constraints): void
    {
        $this->errors->addAll($this->validator->validate($data, new Collection($constraints)));

        if ($this->hasErrors()) {
            throw new ValidationException($this->getErrorsAsArray());
        }
    }

    public function hasErrors(): bool
    {
        return $this->errors->count() > 0;
    }

    public function isValid(): bool
    {
        return !$this->hasErrors();
    }

    public function getErrorsAsArray(): array
    {
        $errorMessages = [];
        foreach ($this->errors as $violation) {
            preg_match('/\[(.*?)\]/', $violation->getPropertyPath(), $match);
            $errorMessages[$match[1]][] = $violation->getMessage();
        }

        return $errorMessages;
    }
}