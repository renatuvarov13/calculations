<?php

declare(strict_types=1);

namespace App\Infrastructure\ParamConverter;

use App\Application\Command\CreateCalculation\Command;
use App\Domain\Entity\Operation;
use App\Infrastructure\Service\Validator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class CalculationParamConverter implements ParamConverterInterface
{
    public function __construct(private readonly Validator $validator)
    {
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $this->validate($request->request->all());

        $request->attributes->set($configuration->getName(), new Command(
            (float)$request->get('number1'),
            (float)$request->get('number2'),
            strtolower(($request->get('operation'))),
        ));

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === Command::class;
    }

    private function validate(array $requestBody): void
    {
        $this->validator->validate($requestBody, [
            'number1' => [
                new NotBlank(),
                new Type(type: 'numeric'),
            ],
            'number2' => [
                new NotBlank(),
                new Type(type: 'numeric'),
            ],
            'operation' => [
                new NotBlank(),
                new Choice(
                    callback: [Operation::class, 'values'],
                    message: 'Unexpected operation type.'
                ),
            ],
        ]);
    }
}