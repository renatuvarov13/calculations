<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Repository;

use App\Application\Persistence\Repository\CalculationRepositoryInterface;
use App\Domain\Entity\Calculation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CalculationRepository extends ServiceEntityRepository implements CalculationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Calculation::class);
    }

    public function add(Calculation $calculation)
    {
        $this->_em->persist($calculation);
        $this->_em->flush();
    }

    public function getNew(): array
    {
        return $this->findBy(['completed' => false]);
    }

    public function complete(array $calculationIds): void
    {
        $this->createQueryBuilder('c')
            ->update(Calculation::class, 'c')
            ->set('c.completed', 'true')
            ->where('c.id IN (:calculationIds)')
            ->setParameter('calculationIds', $calculationIds)
            ->getQuery()
            ->execute();
    }

    public function findById(string $id): Calculation
    {
        return $this->find($id);
    }

    public function getAll(): array
    {
        return $this->findAll();
    }
}