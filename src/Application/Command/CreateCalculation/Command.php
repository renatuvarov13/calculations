<?php

declare(strict_types=1);

namespace App\Application\Command\CreateCalculation;

class Command
{
    public function __construct(
        public readonly float $number1,
        public readonly float $number2,
        public readonly string $operation,
    ) {
    }
}