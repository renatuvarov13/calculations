<?php

declare(strict_types=1);

namespace App\Application\Command\CreateCalculation;

use App\Application\Persistence\Repository\CalculationRepositoryInterface;
use App\Application\Service\UuidGeneratorInterface;
use App\Domain\Entity\Calculation;
use App\Domain\Entity\Operation;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class Handler implements MessageHandlerInterface
{
    public function __construct(
        private readonly CalculationRepositoryInterface $calculations,
        private readonly UuidGeneratorInterface $uuidGenerator,
    ) {
    }

    public function __invoke(Command $command): string
    {
        $calculation = new Calculation(
            $this->uuidGenerator->generate(),
            $command->number1,
            $command->number2,
            Operation::from($command->operation),
        );

        $this->calculations->add($calculation);

        return $calculation->getId();
    }
}