<?php

declare(strict_types=1);

namespace App\Application\Persistence\Repository;

use App\Domain\Entity\Calculation;

interface CalculationRepositoryInterface
{
    public function add(Calculation $calculation);

    /**
     * @return Calculation[]
     */
    public function getNew(): array;

    public function complete(array $calculationIds): void;

    public function findById(string $id): Calculation;

    /**
     * @return Calculation[]
     */
    public function getAll(): array;
}
