<?php

declare(strict_types=1);

namespace App\Application\EventListener;

use App\Application\Persistence\Repository\CalculationRepositoryInterface;
use App\Domain\Event\CalculationWasCompleted;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: CalculationWasCompleted::class, method: 'onComplete')]
class CalculationWasCompletedListener
{
    public function __construct(private readonly CalculationRepositoryInterface $calculations)
    {
    }

    public function onComplete(CalculationWasCompleted $event): void
    {
        $this->calculations->complete($event->getCalculationIds());
    }
}