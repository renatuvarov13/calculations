<?php

declare(strict_types=1);

namespace App\Application\Query\GetCalculations;

class Dto
{
    public function __construct(
        public readonly string $id,
        public readonly float $number1,
        public readonly float $number2,
        public readonly string $operation,
        public readonly ?float $total,
        public readonly ?string $message,
    ) {
    }
}