<?php

declare(strict_types=1);

namespace App\Application\Query\GetCalculations;

use App\Application\Persistence\Repository\CalculationRepositoryInterface;
use App\Domain\Event\CalculationWasCompleted;
use ArithmeticError;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class Handler implements MessageHandlerInterface
{
    public function __construct(
        private readonly CalculationRepositoryInterface $calculations,
        private readonly EventDispatcherInterface $eventDispatcher,
    ) {
    }

    /**
     * @return Dto[]
     */
    public function __invoke(Query $query): array
    {
        $calculations = [];
        foreach ($this->calculations->getNew() as $calculation) {
            try {
                $total = $calculation->calculate();
            } catch (ArithmeticError $e) {
                $message = "Error: {$e->getMessage()}";
            } finally {
                $calculationData = new Dto(
                    $calculation->getId(),
                    $calculation->getNumber1(),
                    $calculation->getNumber2(),
                    $calculation->getOperation()->value,
                    $total ?? null,
                    $message ?? null,
                );

                unset($total, $message);
            }

            $calculations[] = $calculationData;
        }

        $this->eventDispatcher->dispatch(new CalculationWasCompleted(
            array_map(fn(Dto $dto): string => $dto->id, $calculations)
        ));

        return $calculations;
    }
}