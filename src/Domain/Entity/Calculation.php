<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use ArithmeticError;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'calculations')]
class Calculation
{
    #[ORM\Id]
    #[ORM\Column(type: 'string')]
    #[ORM\GeneratedValue(strategy: 'NONE')]
    private string $id;

    #[ORM\Column(name: 'number_1', type: 'float', updatable: false)]
    private float $number1;

    #[ORM\Column(name: 'number_2', type: 'float', updatable: false)]
    private float $number2;

    #[ORM\Column(type: 'string', updatable: false, enumType: Operation::class)]
    private Operation $operation;

    #[ORM\Column(type: 'boolean')]
    private bool $completed = false;

    public function __construct(
        string $id,
        float $number1,
        float $number2,
        Operation $operation,
    ) {
        $this->id = $id;
        $this->number1 = $number1;
        $this->number2 = $number2;
        $this->operation = $operation;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNumber1(): float
    {
        return $this->number1;
    }

    public function getNumber2(): float
    {
        return $this->number2;
    }

    public function getOperation(): Operation
    {
        return $this->operation;
    }

    /**
     * @throws ArithmeticError
     */
    public function calculate(): float
    {
        return match ($this->operation) {
            Operation::PLUS => $this->number1 + $this->number2,
            Operation::MINUS => $this->number1 - $this->number2,
            Operation::TIMES => $this->number1 * $this->number2,
            Operation::DIVIDED_BY => $this->number1 / $this->number2,
        };
    }

    public function isCompleted(): bool
    {
        return $this->completed;
    }

    public function complete(): void
    {
        $this->completed = true;
    }
}