<?php

declare(strict_types=1);

namespace App\Domain\Entity;

enum Operation: string
{
    case PLUS = 'plus';
    case MINUS = 'minus';
    case TIMES = 'times';
    case DIVIDED_BY = 'divided by';

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
