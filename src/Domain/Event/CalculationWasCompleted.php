<?php

declare(strict_types=1);

namespace App\Domain\Event;

class CalculationWasCompleted
{
    public function __construct(private readonly array $calculationIds)
    {
    }

    public function getCalculationIds(): array
    {
        return $this->calculationIds;
    }
}